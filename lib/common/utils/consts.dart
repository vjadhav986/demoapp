import 'package:flutter/material.dart';

const kFontFamily = 'kanit';
const kSize0 = 0.0;
const kSize1 = 1.0;
const kSize2 = 2.0;
const kSize4 = 4.0;
const kSize5 = 5.0;
const kSize6 = 6.0;
const kSize8 = 8.0;
const kSize9 = 9.0;
const kSize10 = 10.0;
const kSize12 = 12.0;
const kSize14 = 14.0;
const kSize15 = 15.0;
const kSize16 = 16.0;
const kSize18 = 18.0;
const kSize20 = 20.0;
const kSize22 = 22.0;
const kSize23 = 23.0;
const kSize24 = 24.0;
const kSize32 = 32.0;
const kSize36 = 36.0;
const kSize40 = 40.0;
const kSize44 = 44.0;
const kSize43 = 43.0;
const kSize72 = 72.0;
const kSize9_33 = 9.33;
const kSize100 = 100.0;
const kSize58 = 58.0;
const kSize68 = 68.0;
const kSize88 = 88.0;
const kSize158_5 = 158.5;
const kSize82 = 82.0;
const kSize400 = 400.0;
const kSize150 = 150.0;
const kSize330 = 330.0;
const kSize261 = 261.0;
const kSize192 = 192.0;
const kSize123 = 123.0;
const kSize54 = 54.0;
const kSize0_5 = 0.5;
const kSize77 = 77.0;
const kSize120 = 120.0;

const kTotalReview = 10;
const kPaddingHor0 = EdgeInsets.symmetric(horizontal: kSize0);
const kPaddingVert0 = EdgeInsets.symmetric(vertical: kSize0);
const kPaddingVert4 = EdgeInsets.symmetric(vertical: kSize8);
const kPaddingHori4 = EdgeInsets.symmetric(horizontal: kSize4);
const kPaddingAll8 = EdgeInsets.all(kSize8);
const kPaddingHori8 = EdgeInsets.symmetric(horizontal: kSize8);
const kPaddingVert8 = EdgeInsets.symmetric(vertical: kSize8);
const kPaddingAll24 = EdgeInsets.all(kSize24);
const kPaddingHori24 = EdgeInsets.symmetric(horizontal: kSize24);
const kPaddingVert24 = EdgeInsets.symmetric(vertical: kSize24);
const kPaddingAll16 = EdgeInsets.all(kSize16);
const kPaddingHori16 = EdgeInsets.symmetric(horizontal: kSize16);
const kPaddingVert16 = EdgeInsets.symmetric(vertical: kSize16);
const kPaddingAll20 = EdgeInsets.all(kSize20);
const kPaddingHori20 = EdgeInsets.symmetric(horizontal: kSize20);
const kPaddingVert20 = EdgeInsets.symmetric(vertical: kSize20);
const kMarginHori158_5 = EdgeInsets.symmetric(horizontal: kSize158_5);

const kBorderRad12 = BorderRadius.all(Radius.circular(kSize12));
const kPaddingHori24Vert16 =
    EdgeInsets.symmetric(horizontal: kSize24, vertical: kSize16);

const kPaddingHori24Vert20 =
    EdgeInsets.symmetric(horizontal: kSize24, vertical: kSize20);
const kSize48 = 48.0;

const kFontSize18 = 18.0;
const kFontSize22 = 22.0;
const kFontSize36 = 36.0;

const kLetterSpacing25 = 0.25;
const kInt2 = 2;
const kInt4 = 4;
const kSize30 = 30.0;
const kSize67 = 67.0;
const kSize7 = 7.0;

const kDuration225 = Duration(milliseconds: 225);
const kDuration250 = Duration(milliseconds: 250);
const kDuration275 = Duration(milliseconds: 275);
const kDuration300 = Duration(milliseconds: 300);
const kDuration325 = Duration(milliseconds: 325);

const kBuddhistYearOffsetCalendar = 543;
const kSize80 = 80.0;
const kSize328 = 328.0;
const kSize4Point8 = 4.8;
const kSize13 = 13.0;
const kSize11 = 11.0;
const kSize34 = 34.0;

const BoxShadow kTextHardShadow = BoxShadow(
  blurRadius: 2,
  color: Color(0x80000000),
  offset: Offset(0, 1),
);
