import 'dart:math';

import 'package:badges/badges.dart';
import 'package:demo_app/common/utils/app_theme.dart';
import 'package:demo_app/modules/home/view_model/home_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}

class HomeState extends State<HomeScreen> {
  List<HomeViewModel> dataList = <HomeViewModel>[];
  bool isLoading = false;
  int pageCount = 1;
  int cardItem = 0;
  late ScrollController _scrollController;

  @override
  void initState() {
    super.initState();

    ////LOADING FIRST  DATA
    addItemIntoLisT(1);

    _scrollController = ScrollController(initialScrollOffset: 5.0)
      ..addListener(_scrollListener);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Home',
          style: AppTheme.kHeading2,
        ),
        automaticallyImplyLeading: false,
        backgroundColor: Colors.blue,
        actions: [
          InkWell(
            onTap: () {},
            child: Ink(
              child: Container(
                padding: const EdgeInsets.all(12),
                child: Badge(
                  badgeContent: Text('$cardItem'),
                  child: const Icon(Icons.shopping_cart),
                ),
              ),
            ),
          )
        ],
      ),
      body: GridView.builder(
        controller: _scrollController,
        shrinkWrap: true,
        itemCount: dataList.length,
        gridDelegate:
            const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              print('${dataList[index].id}');
            },
            child: Card(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Prize : ${dataList[index].prize}",
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Size : ${dataList[index].size}",
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  //// ADDING THE SCROLL LISTINER
  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      setState(() {
        isLoading = true;

        if (isLoading) {
          pageCount = pageCount + 1;
          addItemIntoLisT(pageCount);
        }
      });
    }
  }

  void addItemIntoLisT(var pageCount) {
    var prize = [50, 70, 60, 100, 200, 300, 400, 500, 600, 700, 800];
    var size = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200];
    final _random = Random();

    for (int i = (pageCount * 10) - 10; i < pageCount * 10; i++) {
      dataList.add(
        HomeViewModel(
          id: i,
          size: size[_random.nextInt(size.length)],
          prize: prize[_random.nextInt(prize.length)],
        ),
      );
      isLoading = false;
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
