class LoginViewModel {
  LoginState? loginState;

  String? emailErrorMsg;
  String? passwordErrorMsg;
  bool? isvalidEmail;
  bool? isValidPassword;
  LoginViewModel({
    this.loginState = LoginState.initial,
    this.emailErrorMsg = '',
    this.passwordErrorMsg = '',
    this.isvalidEmail = false,
    this.isValidPassword = false,
  });
}

enum LoginState {
  initial,
  loading,
  success,
}
