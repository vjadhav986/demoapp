import 'dart:async';

import 'package:demo_app/common/utils/app_colors.dart';
import 'package:demo_app/common/utils/app_theme.dart';
import 'package:demo_app/common/utils/consts.dart';
import 'package:demo_app/core/app_routes.dart';
import 'package:demo_app/core_components/bloc_builder.dart';
import 'package:demo_app/modules/login/bloc/login_bloc.dart';
import 'package:demo_app/modules/login/view_model/login_view_model.dart';
import 'package:flutter/material.dart';

const String _loginText = 'Login';
const String _userNameText = 'User Name';
const String _passwordText = 'Password';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginBloc loginBloc = LoginBloc();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: loginBloc,
      builder: () {
        return loginBloc.state.loginState == LoginState.loading
            ? const Center(child: CircularProgressIndicator())
            : Scaffold(
                appBar: AppBar(
                  title: const Text(
                    _loginText,
                    style: AppTheme.kHeading2,
                  ),
                  automaticallyImplyLeading: false,
                  backgroundColor: Colors.blue,
                ),
                body: buildForm(),
              );
      },
    );
  }

  Widget buildForm() {
    return Padding(
      padding: const EdgeInsets.all(kSize20),
      child: BlocBuilder(
        bloc: loginBloc,
        builder: () {
          return ListView(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(kSize10),
                child: TextField(
                  onChanged: (inputValue) {
                    loginBloc.validateEmailClicked(inputValue);
                  },
                  controller: _emailController,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: _userNameText,
                    errorText: loginBloc.state.isvalidEmail!
                        ? null
                        : loginBloc.state.emailErrorMsg,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(
                    kSize10, kSize10, kSize10, kSize0),
                child: TextField(
                  onChanged: (inputValue) {
                    loginBloc.validatePassword(inputValue);
                  },
                  obscureText: true,
                  controller: _passwordController,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: _passwordText,
                    errorText: loginBloc.state.isValidPassword!
                        ? null
                        : loginBloc.state.passwordErrorMsg,
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                  height: 50,
                  padding: const EdgeInsets.fromLTRB(
                      kSize10, kSize0, kSize10, kSize0),
                  child: ElevatedButton(
                    child: Text(
                      _loginText,
                      style: AppTheme.kHeading2
                          .copyWith(color: AppColors.kLight100),
                    ),
                    onPressed: (loginBloc.state.isvalidEmail! &&
                            loginBloc.state.isValidPassword!)
                        ? () {
                            _handleLogin(loginBloc);
                          }
                        : null,
                  )),
              const SizedBox(
                height: kSize40,
              ),
            ],
          );
        },
      ),
    );
  }

  void _handleLogin(LoginBloc loginBloc) async {
    loginBloc.callLoginApi();

    Timer(
      const Duration(seconds: 3),
      () {
        Navigator.pushNamed(
          context,
          AppRoutes.homeScreen,
        );
      },
    );
  }
}
