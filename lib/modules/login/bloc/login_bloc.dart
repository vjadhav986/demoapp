import 'dart:async';

import 'package:demo_app/core_components/bloc.dart';
import 'package:demo_app/modules/login/view_model/login_view_model.dart';

class LoginBloc extends Bloc<LoginViewModel> {
  void validateEmailClicked(String email) {
    if (email.isValidEmail()) {
      state.isvalidEmail = true;
      emit(state);
    } else {
      if (email.length > 4) {
        state.emailErrorMsg = 'please enter a valid email id';
        state.isvalidEmail = false;
        emit(state);
      }
    }
  }

  void validatePassword(String password) {
    if (password.length >= 8) {
      if (password.isValidPassword()) {
        state.isValidPassword = true;
        emit(state);
      } else {
        state.isValidPassword = false;
        state.passwordErrorMsg = 'please enter alphanumeric character';
        emit(state);
      }
    } else {
      state.isValidPassword = false;
      state.passwordErrorMsg = 'password should be 8';
      emit(state);
    }
  }

  void callLoginApi() {
    state.loginState = LoginState.loading;
    emit(state);

    Timer(
      const Duration(seconds: 3),
      () {
        state.loginState = LoginState.success;
        emit(state);
      },
    );
  }

  @override
  LoginViewModel initDefaultValue() {
    return LoginViewModel();
  }
}

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }

  bool isValidPassword() {
    return RegExp(
            r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
        .hasMatch(this);
  }
}
