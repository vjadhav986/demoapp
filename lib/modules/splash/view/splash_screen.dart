import 'dart:async';

import 'package:demo_app/core/app_routes.dart';
import 'package:flutter/material.dart';

const _splashScreenAsset = 'assets/images/default_bg_image.png';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(
      const Duration(milliseconds: 1000),
      () => pushToLoginScreen(),
    );

    super.initState();
  }

  void pushToLoginScreen() {
    Navigator.pushNamed(
      context,
      AppRoutes.loginScreen,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          _defaultImage(),
        ],
      ),
    );
  }

  Widget _defaultImage() {
    return Image.asset(
      _splashScreenAsset,
      fit: BoxFit.cover,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
    );
  }
}
