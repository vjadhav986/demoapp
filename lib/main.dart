import 'package:demo_app/core/app_routes.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Demo App',
      builder: (context, child) {
        final MediaQueryData data = MediaQuery.of(context);
        return MediaQuery(
          data: data.copyWith(
            textScaleFactor: 1,
          ), // To ignore mobile's text scaling settings
          child: child ?? Container(),
        );
      },
      theme: ThemeData(
        primarySwatch: Colors.purple,
        // fontFamily: kFontFamily,
        // canvasColor: AppColors.kLight100,
        // splashColor: AppColors.kSecondarySplash,
        // hoverColor: AppColors.kSecondarySplash,
        // highlightColor: AppColors.kSecondarySplash,
      ),
      initialRoute: AppRoutes.splashScreen,
      routes: AppRoutes.getRoutes(),
    );
  }
}
