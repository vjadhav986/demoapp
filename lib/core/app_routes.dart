import 'package:demo_app/modules/home/view/home_screen.dart';
import 'package:demo_app/modules/login/view/login_screen.dart';
import 'package:demo_app/modules/splash/view/splash_screen.dart';
import 'package:flutter/material.dart';

class AppRoutes {
  // Route name constants
  static const String splashScreen = '/splashScreen';
  static const String loginScreen = '/loginScreen';
  static const String homeScreen = '/homeScreen';

  /// The map used to define our routes, needs to be supplied to [MaterialApp]
  static Map<String, WidgetBuilder> getRoutes() {
    return {
      splashScreen: (context) => const SplashScreen(),
      loginScreen: (context) => const LoginScreen(),
      homeScreen: (context) => const HomeScreen()
    };
  }
}
